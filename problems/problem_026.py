# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    sum = 0
    for grades in values:
        sum += grades
    avg = sum/len(values)
    if avg >= 90:
        return "A"
    if avg >= 80 and avg <90:
        return "B"
    if avg >=70 and avg <80:
        return "C"
    if avg >=60 and avg <70:
        return "D"
    else:
        return "F"
