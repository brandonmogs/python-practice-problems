# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    max = 0
    if values == []:
        return None
    for nums in values:
        if nums > max:
            max = nums
    return max
